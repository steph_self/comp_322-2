#include <stdio.h>
#include <stdlib.h>

/* Declare dynamic arrays and global variables*/
int *resources = NULL;
int *available = NULL;
int *max_claim = NULL;
int *allocate = NULL;
int *need = NULL;

int num_processes;
int num_resources;

/***********************************************************/
// "PROCEDURE TO PRINT RESOURCE UNITS ARRAY"

void  print_resource_array() {

	/* declare local variables */
	int i;

	/* for loop: print each resource index */
	printf("Resources: \n");
	for(i =0; i<num_resources; i++){
		printf("\t r%d", i);
	}
		printf("\n");
	
	/* for loop: print number of total units of each resource index */
		for(i =0; i<num_resources; i++){
			printf("\t%d", resources[i] );
		}
		printf("\n");
	return;
}


/***********************************************************/
//"PROCEDURE TO PRINT AVAILABLE RESOURCES ARRAY" 

void print_available_array() {

	/* declare local variables */
	int j;

	/* for loop: print each resource index */
	printf("Available: \n");
	for(j =0; j<available; j++){
		printf("\t r%d", j);
	}
		printf("\n");

	/* for loop: print number of available units of each resource index */
		for(j =0; j<available; j++){
			printf("\t%d", resources[j] );
		}
		printf("\n");
	return;
}


/***************************************************************/
//"PROCEDURE TO PRINT MAX CLAIM ARRAY"

void print_max_claim() { 

	/* declare local variables */
	int i,j;

	/* for loop: print each resource index */
	printf("Max Claim: \n");
	for(i=0; i<resources; i++){
		printf("\tr%d", i);
	}
	printf("\n");
	/* for each process: i=0 to num processes-1 */
		/* for each resource: j=0 to num resources-1 */
	for(i=0; i<num_processes-1;i++){
		printf("p%d\t", i);
		for(j=0; j<num_resources-1; j++){
			printf("%d\t",allocate[i * num_resources + j]);
		}
	}
	printf("\n");
			/* print maximum number of units each process may request from each resource */
	printf("%d\t", max_claim[i * num_resources + j]);
	printf("\n");
	return;
}


/***************************************************************/
//"PROCEDURE TO PRINT ALLOCATION ARRAY"

void print_allocation_array() {

	/* declare local variables */
	int i,j;

	/* for loop: print each resource index */
	printf("Allocated: \n");
	for(i=0; i<num_resources; i++){
		printf("\tr%d", i);
	}
	printf("\t");
	printf("\n");

	/* for each process: */
		/* for each resource: */
	for(i=0; i<num_processes; i++){
		printf("p%d\t", i);
		for(j=0; j<num_resources; j++){
			printf("%d\t",allocate[i * num_resources + j]);
		}
		printf("\n");
	}
	printf("\n");

		
			/* print maximum number of units each process is allocated from each resource */
	printf("%d\t", allocate[i * num_resources + j]);
	printf("\n");
	
	return;
}

/***************************************************************/
//"PROCEDURE TO PRINT NEED ARRAY"

void print_need_array() {
	/* declare local variables */
	int i,j;
	/* for loop: print each resource index */
	printf("Need: \n");
	for(i=0; i<num_resources; i++){
		printf("\tr%d", i);
	}
	printf("\n");
	/* for each process: */
		/* for each resource: */
			/* print number of units needed by each process from each resource */
	for(i=0; i<num_processes; i++){
		printf("p%d\t", i);
		for(j=0; j<num_resources; j++){
			printf("%d\t",allocate[i * num_resources + j]);
		}
		printf("\n");
	}
	printf("\n");
	return;
}


/****************************************************************/
void option1() {

	/* declare local variables */
	int i;
	int j;
	int units;

	/* prompt for number of processes and number of resources */
	printf("Enter selection: \n");
	scanf("%d", &num_processes);
	printf("Enter number of processes: \n");
	scanf("%d", &num_processes);

	/* allocate memory for arrays and arrays: resource, available, max_claim, allocated, need */
	resources = (int *)malloc(num_resources * sizeof(int));
	available = (int *)malloc(num_resources * sizeof(int));
	max_claim = (int *)malloc(num_processes * num_resources * sizeof(int));
	allocate = (int *)malloc(num_processes * num_resources * sizeof(int));
	need = (int *)malloc(num_processes * num_resources * sizeof(int));

	/* for each resource, prompt for number of units, set resource and available arrays indices*/
	for(i=0; i< num_resources; i++){
		printf("Enter number of units for resources r%d: ", i );
		scanf("%d", &units);
		printf("%d", units);
		resources[i] = units;
		available[i] = units;
	}
	printf("\n");

	/* for each process, for each resource, prompt for maximum number of units requested by process, update max_claim and need arrays */ 
	for(i=0; i< num_processes; i++){
		for(j=0; j< num_resources; j++){
		printf("Enter maximum number of units process p%d will request from resource r%d: ", i);
		scanf("%d", &units);
		printf("%d", units);
		max_claim[i * num_resources +j] = units;
		need[i * num_resources + j] = units;
	}
}
	printf("\n");
	/* for each process, for each resource, prompt for number of resource units allocated to process */
	for(i=0; i< num_processes; i++){
		for(j=0; j< num_resources; j++){
		printf("Enter number of units of resource r%d allocated to process p%d: ", i );
		scanf("%d", &units);
		
		allocate[i * num_resources + j] = units;
		need[i * num_resources + j] -= units;
		available[j] -= units;
	}
}
	printf("\n");
	/* call procedures to print resource array, available array, max_claim array, allocated array, need array */
	print_resource_array();
	print_available_array();
	print_max_claim();
	print_allocation_array();
	print_need_array();
	
	return;
}


/********************************************************************/
void option2() {
 
	/* declare local variables, including array to indicate if process is safely sequenced and "done" counter*/
	int done =0;
	int i,j,less;
	int *sequenced = (int *)calloc(num_processes, sizeof(int));

	/* while not done */
	while(done < num_processes){

		/* for each process to check for safe sequencing */
		for(i=0; i<num_processes; i++){
			less = 1;
			/* if not already put in a safe sequence */
			if(sequenced[i] == 0){
				printf("Checking p%d < ",i );

			
				/* for each resource, print need array entry */
			for (j = 0; j < num_resources; j++){
				printf("%d ", need[i * num_resources + j]);
			}
				printf("> <= < ");
				/* for each resource, print available array entry */
				for( j=0; j<num_resources; j++){
					printf("%d ",available[j]);
				}
				printf(">");
				/* for each resource, check if need array entry is less than available array entry */
				for( j=0; j<num_resources; j++){
				  less = less && (need[i * num_resources + j]) <= available[j];
                }
                
				/* if all need entries are less than the corresponding available entry */ 
				if(less == 1){
					printf(" :p %d safely sequenced ", i);

					/* put process in safe sequence */
					 sequenced[i] =1;

					/* for each resource */
					 for( j=0; j<num_resources; j++){

						/* adjust units in available array */
						available[j] += allocate[i * num_resources + j];

						/* reset units in allocated array */
						allocate[i * num_resources + j] = 0;

						/* increment "done" counter */
						done++;
					}
					
				}
			}
		}
		
	}
	free(sequenced);
	printf("\n");
	return;
}


/********************************************************************/
void option3() {
	
	/* free memory for all arrays and arrays is not NULL */
	return;
}


/***************************************************************/
int main() {
	/* Declare local variables */	
	int choice;
	/* Until the user quits, print the menu, get user's choice, call the appropriate procedure */
	while (choice != 3) {
     
      printf("1) Enter parameters\n");
      printf("2) Determine safe sequence\n");
      printf("3) Quit program \n");
      scanf("%d",&choice);
      printf("Enter selection:\n"); 
   

      switch (choice){
         case 1 :
            option1();break;
         case 2 :
            option2();break;
         case 3 : option3();
            printf("Quitting program\n" ); break;
      } 
   }
	return 1;
}


